import 'package:flutter/material.dart';
import 'package:gigo_clean/constants/constants.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.5,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("download.png"),
            ),
          ),
        ),
        const Text(
            "Get read to make your life easy with single click \n of app. Which makes your cleaning easy"),
        Center(
          child: Container(
            height: MediaQuery.of(context).size.height * 0.1,
            width: MediaQuery.of(context).size.width,
            color: Colors.red,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const CircleAvatar(
                  backgroundColor: ksecondarColor,
                  child: Icon(Icons.person),
                ),
                Column(
                  children: const [Text("User"), Text("Are you a customer?")],
                )
              ],
            ),
          ),
        )
      ]),
    );
  }
}
