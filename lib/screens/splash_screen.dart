import 'package:flutter/material.dart';
import 'package:gigo_clean/constants/constants.dart';
import 'package:gigo_clean/screens/login_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.5,
              width: MediaQuery.of(context).size.width,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("download.png"),
                ),
              ),
            ),
            FloatingActionButton.extended(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginScreen()));
                },
                backgroundColor: kprimaryColor,
                label: Row(
                  children: const [
                    Text("Get Started"),
                    Icon(Icons.arrow_right)
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
